// Copyright (c) 2019 Milo Gilad
// 
// This software is released under the MIT License.
// https://opensource.org/licenses/MIT

const path = require('path');

module.exports = [{
  entry: './src/js/index.ts',
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/
      }
    ]
  },
  resolve: {
    extensions: [ '.tsx', '.ts', '.js' ]
  },
  output: {
    filename: 'index.js',
    path: path.resolve(__dirname, 'public/')
  }
}];
