build: html webpack

html: yarn
	cp -rf src/html/. public/

webpack: yarn lint
	npx webpack-cli

lint:
	npx tslint -c tslint.json --fix 'src/js/**/*.ts'

yarn:
	yarn install --production=false

clean:
	rm -rf public || mkdir public
